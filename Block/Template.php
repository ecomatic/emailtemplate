<?php
/**
 * Created by PhpStorm.
 * User: Yuki
 * Date: 14/03/2018
 * Time: 10:52
 */

namespace Ecomatic\EmailTemplate\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class Template extends \Magento\Framework\View\Element\Template {

	protected $_storeManager;
	protected $_currency;
	protected $_ruleRepositoryInterface;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Directory\Model\Currency $currency,
		\Magento\SalesRule\Api\RuleRepositoryInterface $ruleRepositoryInterface,
		array $data = []
	) {
		$this->_storeManager = $context->getStoreManager();
		$this->_currency     = $currency->load($this->getCurrentCurrencyCode());
		$this->_ruleRepositoryInterface = $ruleRepositoryInterface;
		parent::__construct($context, $data);
	}

	/**
	 * Get store base currency code
	 *
	 * @return string
	 */
	public function getBaseCurrencyCode() {
		return $this->_storeManager->getStore()->getBaseCurrencyCode();
	}

	/**
	 * Get current store currency code
	 *
	 * @return string
	 */
	public function getCurrentCurrencyCode() {
		return $this->_storeManager->getStore()->getCurrentCurrencyCode();
	}

	/**
	 * Get default store currency code
	 *
	 * @return string
	 */
	public function getDefaultCurrencyCode() {
		return $this->_storeManager->getStore()->getDefaultCurrencyCode();
	}

	/**
	 * Get allowed store currency codes
	 *
	 * If base currency is not allowed in current website config scope,
	 * then it can be disabled with $skipBaseNotAllowed
	 *
	 * @param bool $skipBaseNotAllowed
	 *
	 * @return array
	 */
	public function getAvailableCurrencyCodes( $skipBaseNotAllowed = false ) {
		return $this->_storeManager->getStore()->getAvailableCurrencyCodes( $skipBaseNotAllowed );
	}

	/**
	 * Get array of installed currencies for the scope
	 *
	 * @return array
	 */
	public function getAllowedCurrencies() {
		return $this->_storeManager->getStore()->getAllowedCurrencies();
	}

	/**
	 * Get current currency rate
	 *
	 * @return float
	 */
	public function getCurrentCurrencyRate() {
		return $this->_storeManager->getStore()->getCurrentCurrencyRate();
	}

	/**
	 * Get currency symbol for current locale and currency code
	 *
	 * @return string
	 */
	public function getCurrentCurrencySymbol() {
		return $this->_currency->load($this->getCurrentCurrencyCode())->getCurrencySymbol();
	}

	public function getFormattedPrice( $price ) {
		return $this->_currency->format($price, array('precision' => 0), false, false);
	}

	/**
	 * @return \Magento\SalesRule\Api\Data\RuleInterface[]
	 */
	public function getOrderDiscounts() {
		/* @var \Magento\Sales\Model\Order $order */
		$order = $this->getData('order');
		$ruleIds = explode(',', $order->getAppliedRuleIds());
		$rules = array();
		foreach ( $ruleIds as $id ) {
			try {
				$rules[] = $this->_ruleRepositoryInterface->getById( $id );
			} catch ( NoSuchEntityException $e ) {
			} catch ( LocalizedException $e ) {
			}
		}

		return $rules;
	}
}